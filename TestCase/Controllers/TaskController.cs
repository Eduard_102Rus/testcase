﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestCase.Data.Entities;
using TestCase.Data.Repositories;
using TestCase.Dto;

namespace TestCase.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private  int Id { get; set; }
        private readonly IMapper _mapper;
        private readonly TaskRepository _taskRepository;

        public TaskController(TaskRepository taskRepository, IMapper mapper)
        {
            this._mapper = mapper;
            _taskRepository = taskRepository;
        }


        [HttpPost("Create")]
        public IActionResult Create([FromBody]CaseDto caseDto)
        {
            if (ModelState.IsValid)
            {
                var task = _mapper.Map<CaseDto, Case>(caseDto);

                _taskRepository.Add(task);
                return NoContent();
            }
            return BadRequest();
        }

        [HttpGet("tasks")]
        public IActionResult GetTasks()
        {
            return Ok(_taskRepository.GetAll());
        }

       

        [HttpGet("task/{id}")]
        public IActionResult GetResult(int Id)
        {
           
            return Ok(_taskRepository.Get(Id));
        }

        [HttpPost("delete")]
        public IActionResult DeleteTask([FromBody]int id)
        {
            if(ModelState.IsValid)
            {
                _taskRepository.Delete(id);
                return NoContent();
            }
            return BadRequest();
        }
        
        
        


        [HttpPost("edit")]
        public IActionResult EditTask([FromBody]CaseDto caseDto)
        {
            if(ModelState.IsValid)
            {
                var task = _mapper.Map<CaseDto, Case>(caseDto);

                _taskRepository.Update(task);
                return NoContent();
            }
            return BadRequest();
        }
        
    }
}