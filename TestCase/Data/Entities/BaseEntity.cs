﻿using System.ComponentModel.DataAnnotations;

namespace TestCase.Data.Entities
{
    public class BaseEntity
    {
        [Key]
        public int Id { get; set; }
    }
}