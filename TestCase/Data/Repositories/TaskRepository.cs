﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestCase.Data.Entities;

namespace TestCase.Data.Repositories
{
    public class TaskRepository: GenericRepository<Case>
    {

        public TaskRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }

        public override Case Get(int id)
        {
            return _dbContext
                .Cases
                .FirstOrDefault(x => x.Id == id);
        }
    }
}
