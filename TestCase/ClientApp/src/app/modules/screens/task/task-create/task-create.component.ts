import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { TaskService } from '../task.service';
import { CreateTask } from '../task.model';
import {  Router } from '@angular/router';
import { TaskEdit } from '../task-edit.model';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.css']
})
export class TaskCreateComponent implements OnInit {

  public form: FormGroup;
  
  constructor(private http: HttpClient, private fb: FormBuilder, private taskService: TaskService, private router: Router) {

  }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      isDone: [true],
      deadLine: ['', Validators.required]
    })
  }
  submit() {
    console.log(<CreateTask>this.form.value);
    this.taskService.createTask(<CreateTask>this.form.value).subscribe(_ => {
      this.router.navigateByUrl('/tasks');
    });
  }
}
