import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { TaskService } from '../task.service';
import { TaskEdit } from '../task-edit.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { forEach } from '@angular/router/src/utils/collection';


export class FormInput {
  public lengt: number;

}
@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.css']
})
export class TaskEditComponent implements OnInit {

  loadedData: boolean = false;
  task: any;
  private id: number;

  public form: FormGroup;
  constructor(private http: HttpClient, private fb: FormBuilder, private taskService: TaskService, private route: ActivatedRoute, private router: Router) {
    
    
  }

  ngOnInit() {

    this.id = this.route.snapshot.queryParams['id'];
    console.log(this.id);
    this.taskService.taskEditId(this.id).subscribe(resp => {
      this.task = resp;
      this.loadedData = true;
      console.log(this.task);
    });


     this.form = this.fb.group({
      id: [this.id],
      name: ['', Validators.required],
      description: ['', Validators.required],
      isDone: [''],
      deadLine: ['', Validators.required]
    })
  }
  submit() {
    
    console.log(<TaskEdit>this.form.value);
    this.taskService.taskEdit(<TaskEdit>this.form.value).subscribe(_ => {
      this.router.navigateByUrl('/tasks');
    })
    

  }
}
